# Jexplore
#### A Very Simple Package Explorer Library for Java Projects

[This library](http://minhaskamal.github.io/Jexplore) can be used as a package or repository explorer. All files/folders from the root directory can be accessed and interacted in a tree structure. 

### How to Use?
1. Simply add <a href="https://github.com/MinhasKamal/Jexplore/raw/master/Jexplore.jar">Jexplore.jar</a> to your project as an External Jar.
2. Now all you need to do is to attach it to your project window. Here is a demo-

```
  public static void main(String[] args) {
		JPanel jPanel = new JPanel();
		jPanel.setLayout(new GridLayout());
		
		FileExplorer fileExplorer = new FileExplorer(); //creating Jexplore object
		fileExplorer.attachTo(jPanel);
		
		JFrame jFrame = new JFrame();
		jFrame.setBounds(10, 5, 500, 700);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setVisible(true);
		jFrame.add(jPanel);
	}
```

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>Jexplore is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
